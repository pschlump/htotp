module gitlab.com/pschlump/htotp

go 1.17

require (
	github.com/atotto/clipboard v0.1.2
	github.com/fatih/structtag v1.2.0 // indirect
	github.com/makiuchi-d/gozxing v0.0.0-20190830103442-eaff64b1ceb7
	github.com/pschlump/Go-FTL v0.7.6 // indirect
	github.com/pschlump/HashStr v0.0.0-20180925204545-f0f66b60e7c5 // indirect
	github.com/pschlump/HashStrings v0.1.0 // indirect
	github.com/pschlump/MiscLib v1.0.0
	github.com/pschlump/filelib v1.0.1
	github.com/pschlump/godebug v1.0.1
	github.com/pschlump/jsonSyntaxErrorLib v1.0.1 // indirect
	github.com/pschlump/jwtverif v0.1.8 // indirect
	github.com/pschlump/mon-alive v0.5.5 // indirect
	github.com/pschlump/ms v1.0.1 // indirect
	github.com/pschlump/radix.v2 v0.2.1 // indirect
	github.com/pschlump/templatestrings v0.1.0 // indirect
	github.com/pschlump/textTemplate v1.12.2 // indirect
	github.com/pschlump/verhoeff_algorithm v1.0.1 // indirect
	github.com/stretchr/testify v1.5.1 // indirect
	github.com/tealeg/xlsx v1.0.5 // indirect
	gitlab.com/pschlump/PureImaginationServer v0.1.41
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543 // indirect
)

replace gitlab.com/pschlump/PureImaginationServer v0.1.41 => ./../../../gitlab.com/pschlump/PureImaginationServer
